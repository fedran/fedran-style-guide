# Fedran Style Guide

This is the Manual of Style for Fedran. It includes rules for consistency, conventions taken, and other elements that may come up during editing or writing.

## Overall

-   By default, use the [Chicago Manual of Style](http://www.chicagomanualofstyle.org/home.html).
-   Books and chapters are single POV.

## Structural

For both books and stories, chapters are broken up by a number of reasons due to the future plans of the aggregate books.

-   If there is more than an hour between paragraphs, these are split into two chapters.
-   If there is a significant location change, split into two.
-   A short story is treated as effectively a single-chapter story.
-   There are never scene breaks.
-   If possible, the name of the POV character should be the first name used in a chapter.

### Epigraphs

All chapters start with an epigraph. This is the place to introduce world-building that needs to be explained for the chapter while avoiding the "as everyone in the room already knows, the current president is..." type of statements. If a reader is confused by a point or a concept (such as using rods for distance), then it should be introduced in the epigraph in that chapter or earlier if it can't reasonably be added in narrative. An epigraph is exactly one paragraph in length.

If there is something confusing that isn't explained but it doesn't make sense to do so in the narrative, then move it to an epigraph. If there is no educational epigraphs needed, then scene setting is appropriate.

All epigraphs have an attribution from an in-world source. These will be on a separate, right-aligned paragraph with a leading em-dash. In source, this is three dashes in the same line as the quote. (`--- *Clans of the Desert*` in the example below).

> There is a distinction between the clan as the spirit itself and the clan as the members affiliated with the spirit. While many times, they are in agreement, there are times when they are not.
>
> — _Clans of the Desert_

Normal italics for plays, stories, and novels is used. For plays, list the act and scene in parenthesis with Arabic numerals but not italic.

-   Correct: _Queen of the Desert_ (Act 2, Scene 4)

If there is an author and a work, then use commas between them.

-   Correct: Bob, _The Glorious World of Bob_

## Punctuation

### Em-Dash in Epigraphs

The spacing around an "em-dash" in the epigraph is a function of the renderer and doesn't need to be highlighted more than once.

### Interrobang

Yelled questions are only used in dialog and use "!?" as a pair.

-   Correct: !?
-   Incorrect: ?!

### Information

-   https://en.wikipedia.org/wiki/Interrobang

## Dialog Conventions

Fedran uses a number of conventions for handling dialog.

### Stuttering

Stuttering uses hyphens between the parts of the stutter.

-   Correct: "I-I don't know what to do."
-   Incorrect: "I—I don't know what to do." (em-dash)

### Extended Pauses

When a character pauses or uses stalling words (um, eh), these are just represented by ellipses. This should be the single Unicode character (`…`) but in the source files it will be three periods (`...`). Likewise, the ellipses have no leading space but will have a trailing space.

-   Correct: "Look… I like you."
-   Incorrect: "Look …I like you."
-   Incorrect: "Look…I like you."
-   Incorrect: "Look … I like you."

If there is an extended action, ellipses can be at the end of a quoted section (without a trailing space). It will resume with a quoted line with another ellipses, trailing space, and then the rest of the dialog.

-   Correct: "Look…" Bob looked away for a moment. "… I like you."

In this case, there are exactly three periods in the ellipse.

### Trailing Sentences

If a character trails off at the end of their sentences but it is a completed thought, the sentence should have an ellipse and then a period.

-   Correct: "Yeah, I need to go downstairs…."

### Interruptions

Sometimes there are interruptions in dialog, usually in arguments. This is indicated by the em-dash (`—`).

Correct:

> Bob shook his head. "No, I said—"
>
> Gary interrupted. "I don't care what you said!"

If a character continues to keep speaking through an interruption, then their next dialog will start with the em-dash without a trailing space.

> Bob said, "Look, if you are going—"
>
> Gary said, "I'm not getting you anything"
>
> "—to the store, get me some sushi."

### Stressed Words

Stressed words are not indicated by italics or bolds.

-   Correct: "I hope you have a good day!" he said emphaizing the word "good".
-   Correct: "I hope you have a good day!" he said.
-   Incorrect: "I hope you have a _good_ day!" he said.

### Foreign or Unknown Words

When the point of view character doesn't know a word or phrase, it is indicated with italics. If the word is completely unknown to the POV character, it will typically be left in the original non-English version (if possible).

-   Correct: "You should _defenestrate_ yourself out of a high window."
-   Correct: "Do you have a _Kopfschmerzen_?"
-   Correct: "Slow down, _imapatsu_ daughter."

Foreign words are also notationally translated if the POV character knows them but no special markers are given for those.

-   Correct: "Do you have a headache?" she asked in German.
-   Correct: "Slow down, impatient daughter."

This does mean over time, words will cease to be italicized when the character gets more familiar with them. Typically, the italic to non-italic will happen between stories and novels instead of between chapters.

### Telepathy

There is a lot of telepathy in Fedran. Inspired by Diane Duane's _So You Want to be a Wizard_ series, these use guillemet (`«` and `»`) as alternative quote indicators. In the source files, these are `<<` and `>>` respectively. Diane used parenthesis in earlier books, but I liked the look of guillemet's after alternating for a few years.

> «Ouch,» came the mental complaint. It sung in the back of his head, the masculine thoughts of the horse he was grooming intruding directly into his thoughts.
>
> He started to reach up but then stopped. Taking a deep breath, he pressed his other hand against the sun-warmed side of Ryachuikùo’s neck. «I didn’t mean to pull so hard. Forgive me?»

## Proper Nouns

### Possessives

When writing a possessive that ends in "s", use "s's" not "s".

-   Correct: Maris's dress
-   Incorrect: Maris' dress

For justifications, see:

-   https://apvschicago.com/2011/06/apostrophe-s-vs-apostrophe-forming.html
-   [Chicago 7.19](http://www.chicagomanualofstyle.org/book/ed17/part2/ch07/psec019.html)

### Lorban Names

Lorban names are inspired by [Lojban](https://en.wikipedia.org/wiki/Lojban) in their construction.

-   The `c` is a soft `sh` sound, `k` is used for a hard `c` in English.
-   Names start and end in consonants.
-   Names do not have double characters.

Some names are notationally translated so "Brook" is actually "Flecur" but no one wants to figure that out.

### Miwāfu Names

Miwāfu names have a lot of similarities to Japanese but it is not that language. It doesn't share either a dictionary or grammar with Japanese.

-   There are some additional syllables that aren't in the Japanese language.
-   The penultimate syllable has a gender accent:
    -   Acute (`áéíóú`) indicates feminine, precision, and control. Either raise the pitch at the end of the word or say the entire word in a higher pitch.
    -   Grave (`àèìòù`) indicates masculine, power, or large. This is a lower pitch.
    -   Macros (`āēīōū`) indicates neuter, child, or uncountable. Say either the penultimate vowel a beat longer or draw out the word slightly.
-   If you want to say it outloud, break at the vowels. So "Rutejìmo" is "Ru·te·jì·mo".
-   The language has no capitals, but we write with English capitalization for consistency.

Adjectives in proper nouns are more complicated. The name used as an adjective or adverb is not accented.

-   Shimusògo is the name of the clan.
-   Shimusogo Valley is the name of the valley owned by the clan. (Technically, it would be "shimusogo nigìtsu" but it is usually translated.)
-   A shimusogo dépa is a Greater California Road Runner.

## Points of View

As noted earlier, every story or novel is written from a single POV. This means that novels won't switch to a different point of view even between chapters. This POV has an [identifier](https://fedran.com/sources/) for everything from their view. The color scheme is also dependent on the POV (e.g., everything written from Rutejìmo's POV has the same brown color, everything wwritten from Lily's POV has the same blue-green).

In addition, the narrative is written from that specific character's limited view, which makes them an unreliable narrator since their biases and views will alter how they perceive events going on. This also means that they (and therefore the reader) will rarely get the "full" picture of events.

In general, there should be little exposition unless it makes narrative sense (e.g., no "as everyone in the room knows"). Epigraphs are used when information needs to be communicated that the POV doesn't have or wouldn't logically rehash.

### Stereotypes

#### Tarsan

Tarsan is one of the oldest and most "civilized" of the cluster of countries surrounding them. Their views of the world, calendar, system of government, education, and even language have significant influence on Kormar, Gepaul, and other surrounding countries. Because of this, many native to Tarsan see their versions to be superior.

Tarsan High Society is one of examples of this. High Society, when used as a proper noun, is the collective views, reactions, and expectations of members of high society. It is bound together by gossip, infighting, and temporary alliances that it cannot easily be isolated to a few influencers. Because of this, it is frequently treated as being a unique entity and therefore is referenced as "High Society" or "Society" in writing. Rarely is it called "Tarsan High Society" because, from their views, there is only one High Society.
